import csv, re, sys

with open(sys.argv[1], 'r', newline='') as source, open(sys.argv[2], 'w', newline='') as destination:
    reader = csv.reader(source, delimiter=',')
    writer = csv.writer(destination, delimiter=',')
    for row in reader:
        line = ()
        for column in row:
            if re.match("^\d+?\.\d+?$", column) is None:
                line = line + (column, )
            else:
                line = line + ("{0:.4f}".format(float(column)/100), )
        writer.writerow(line)
